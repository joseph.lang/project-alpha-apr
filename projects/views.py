from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from tasks.models import Task
from projects.forms import ProjectForm

# Create your views here.


@login_required
def project_list(request):
    project = Project.objects.filter(owner=request.user)
    context = {"project": project}
    return render(request, "projects/list_projects.html", context)


@login_required
def show_project(request, id):
    project = Project.objects.get(id=id)
    # filters items for list
    # makes every list have unique tasks
    details = Task.objects.filter(project=project)
    context = {
        "details": details,
        "project": project,
    }
    return render(request, "projects/show_project.html", context)


# project = Project.objects.get(id)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()

            return redirect("list_projects")
    else:
        form = ProjectForm()

        context = {
            "create_project": form,
        }
        return render(request, "projects/create_project.html", context)


@ login_required
def delete_project(request, id):
    project = Project.objects.get(id=id)
    if request.method == "POST":
        project.delete()
        return redirect("list_projects")
    context = {
        "project": project
    }
    return render(request, 'projects/delete_project.html', context)
