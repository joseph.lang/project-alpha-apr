from django.shortcuts import get_object_or_404, render, redirect
from tasks.models import Task
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm, NoteForm
from projects.models import Project
# Create your views here.


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save()
            task.save()

            return redirect("list_projects")
    else:
        form = TaskForm()

    context = {
        "create_task": form,
    }
    return render(request, "tasks/create_task.html", context)


@login_required
def create_note(request, id):
    task = get_object_or_404(Task, id=id, assignee=request.user)
    if request.method == "POST":
        form = NoteForm(request.POST, instance=task)
        if form.is_valid():
            note = form.save(False)
            note.task = task
            note.save()

            return redirect('show_my_tasks')
    else:
        form = NoteForm(instance=task)
    context = {
        "form": form,
    }
    return render(request, "tasks/create-note.html", context)


@login_required
def show_my_task(request):
    my_tasks = Task.objects.filter(assignee=request.user)
    context = {"my_tasks": my_tasks}
    return render(request, "tasks/my_tasks.html", context)


@login_required
def delete_task(request, id):
    task = Task.objects.get(id=id)
    if request.method == "POST":
        task.delete()
        return redirect("show_my_tasks")
    context = {
        "task": task
    }
    return render(request, 'tasks/delete-task.html', context)

@login_required
def complete_task(request, id):
    task = Task.objects.get(id=id)
    task.is_completed = True
    task.save()
    return redirect("show_my_tasks")



@login_required
def delete_note(request, id):
    task = get_object_or_404(Task, id=id, assignee=request.user)
    if request.method == "POST":
        task.note.delete()
        return redirect("show_my_tasks")
    return render(request, 'tasks/delete-note.html')
