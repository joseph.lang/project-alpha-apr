# Generated by Django 4.2.7 on 2023-11-17 18:57

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("tasks", "0004_remove_task_note_note_task_alter_note_note_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="task",
            name="note",
            field=models.CharField(blank=True, max_length=65, null=True),
        ),
        migrations.AlterField(
            model_name="note",
            name="note",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="task_note",
                to="tasks.task",
            ),
        ),
    ]
