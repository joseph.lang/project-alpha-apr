# Generated by Django 4.2.7 on 2023-11-17 18:52

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("tasks", "0003_alter_note_note_alter_task_note"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="task",
            name="note",
        ),
        migrations.AddField(
            model_name="note",
            name="task",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="task_notes",
                to="tasks.task",
            ),
        ),
        migrations.AlterField(
            model_name="note",
            name="note",
            field=models.CharField(blank=True, max_length=65, null=True),
        ),
        migrations.AlterField(
            model_name="task",
            name="due_date",
            field=models.DateTimeField(blank=True),
        ),
        migrations.AlterField(
            model_name="task",
            name="start_date",
            field=models.DateTimeField(blank=True),
        ),
    ]
