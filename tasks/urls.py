from django.urls import path
from tasks.views import create_task, show_my_task, create_note, delete_task, delete_note, complete_task

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", show_my_task, name="show_my_tasks"),
    path("create-note/<int:id>/", create_note, name="create_note"),
    path("delete-task/<int:id>/", delete_task, name="delete_task"),
    path("delete-note/<int:id>/", delete_note, name="delete_note"),
    path("complete-task/<int:id>/", complete_task, name="complete_task")
]
