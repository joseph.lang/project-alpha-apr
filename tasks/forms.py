from django import forms
from django.forms import ModelForm
from tasks.models import Task


class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
            "note",
        ]
        widgets = {
            'start_date': forms.DateInput(attrs={'type': 'date'}),
            'due_date': forms.DateInput(attrs={'type': 'date'}),
            'note': forms.TextInput(attrs={'placeholder': 'Optional'}),
            'name': forms.TextInput(attrs={'placeholder': 'Task name'})
        }


class NoteForm(ModelForm):
    class Meta:
        model = Task
        fields = [
            "note",
        ]
        widgets = {
            'note': forms.TextInput(attrs={'placeholder': 'Create a note'}),
        }
